//
//  LoginViewController.swift
//  InfoFox-iPhone
//
//  Created by Dominik De Zordo on 08.02.16.
//  Copyright © 2016 Dominik De Zordo. All rights reserved.
//

import Alamofire
import SwiftyJSON
import UIKit
import NVActivityIndicatorView

class LoginViewController: UIViewController, UITextFieldDelegate {

	@IBOutlet weak var username: UITextField!
	@IBOutlet weak var password: UITextField!
	@IBOutlet weak var loginButton: UIButton!

	@IBAction func loginButtonPressed(sender: AnyObject) {
		login()
	}

    var key:String?
    var loadingView:NVActivityIndicatorView?

	override func viewDidLoad() {
		super.viewDidLoad()
        username.delegate = self
        password.delegate = self
        
        loginButton.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).CGColor
        loginButton.layer.shadowOffset = CGSizeMake(0.0, 2.0)
        loginButton.layer.shadowOpacity = 1.0
        loginButton.layer.shadowRadius = 0.0
        loginButton.layer.masksToBounds = false
        loginButton.layer.cornerRadius = 4.0
        
        let screenSize: CGRect = UIScreen.mainScreen().bounds
        loadingView = NVActivityIndicatorView(frame: CGRect(x: screenSize.width/2-50, y: screenSize.height/2-50, width: 100, height: 100), type: .LineScale, color: UIColor.whiteColor())
        loadingView?.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.75)
        loadingView?.clipsToBounds = true
        loadingView?.layer.cornerRadius = 10
        loadingView?.startAnimation()
	}

	// MARK: - Navigation

	// In a storyboard-based application, you will often want to do a little preparation before navigation
	override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "mainNavigationControllerSegue" {
            let dest = segue.destinationViewController as! (MyNavigationController)
            dest.token = NSUserDefaults.standardUserDefaults().objectForKey("token") as! String
        }
	}

	func login() {
		loginButton.enabled = false
		showProgressBar()
		let params = ["user": username.text!, "pass": password.text!]
		Alamofire.request(.POST, "http://gfhost.htlstp.ac.at/InfoFox/api/token", parameters: params)
			.validate()
			.responseJSON { response in switch response.result {
						case .Success(let json):
						let response = JSON(json)
                        self.key = response["key"].string
						self.loginButton.enabled = true
						self.hideProgressBar()
                        //TODO: Implement Notifications
                        //var delegate = UIApplication.sharedApplication().delegate as! AppDelegate
						dispatch_async(dispatch_get_main_queue()) {
							[unowned self] in
							self.performSegueWithIdentifier("mainNavigationControllerSegue", sender: self)
						}
                        NSUserDefaults.standardUserDefaults().setObject(self.key, forKey: "token")
						break
						case .Failure(_):
                            self.hideProgressBar()
                            let alert = UIAlertController(title: ":(", message: "Ungültige Anmeldedaten", preferredStyle: .Alert)
                            alert.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
                            self.presentViewController(alert, animated: true, completion: nil)
						self.loginButton.enabled = true
						break
					}
			}
	}

	func UIColorFromRGB(rgbValue: UInt) -> UIColor {
		return UIColor(
			red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
			green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
			blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
			alpha: CGFloat(1.0)
		)
	}

	func showProgressBar() {
        self.view.addSubview(loadingView!)
	}

	func hideProgressBar() {
        self.loadingView?.removeFromSuperview()
	}
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {   //delegate method
        textField.resignFirstResponder()
        return true
    }
}
