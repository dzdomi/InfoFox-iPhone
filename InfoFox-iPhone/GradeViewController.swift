//
//  GradeViewController.swift
//  InfoFox-iPhone
//
//  Created by Dominik De Zordo on 05.02.16.
//  Copyright © 2016 Dominik De Zordo. All rights reserved.
//

import UIKit
import Charts
import Fluent

class GradeViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    var subject:Subject!
    let chart:LineChartView = LineChartView()
    
    @IBOutlet weak var gradeTable: UITableView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var totalGrade: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = subject.subjectShort + " - " + subject.teacher
        
        gradeTable.delegate = self
        gradeTable.dataSource = self
        
        gradeTable.rowHeight = UITableViewAutomaticDimension
        gradeTable.estimatedRowHeight = 40
        gradeTable.rowHeight = 40
        
        let line:UIView = UIView(frame: CGRectMake(10, 0, UIScreen.mainScreen().bounds.width - 20, 1))
        line.backgroundColor = UIColorFromRGB(0x3FA9F5)
        bottomView.addSubview(line)
        
        totalGrade.text = getSummary(subject.grades![0].realBewertung)
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: resizeImage(Subject.getIcon(subject.subjectShort), newWidth: (self.navigationController?.navigationBar.bounds.height)! - 20), style: .Plain, target: self, action: "showSubject")
        
        
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return subject.grades!.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("GradeTableViewCell", forIndexPath: indexPath) as! GradeTableViewCell
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd.MM.yyyy"
        
        let row = indexPath.row
        cell.descLabel.text = subject.grades![row].bemerkung
        cell.dateLabel.text = dateFormatter.stringFromDate(subject.grades![row].timestamp)
        cell.gradeLabel.text = subject.grades![row].bewertung
        //cell.gradeLabel.backgroundColor = UIColor.brownColor()
        
        return cell
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        setChart()
        /*let header:UITableViewHeaderFooterView = UITableViewHeaderFooterView()
        header.addSubview(chart)*/
        //chart.bounds = CGRectInset(chart.frame, 10.0, 10.0);
        return chart
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 200
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true) 
        chart.highlightValue(xIndex: (subject.grades?.count)! - indexPath.row - 1, dataSetIndex: 0, callDelegate: false)
    }
    
    func setChart(){
        var dataEntries:[ChartDataEntry] = []
        var xVals = [NSInteger]()
        var count = 0;
        for var i = subject.grades!.count - 1; i >= 0; i-- {
            xVals.append(count)
            dataEntries.append(ChartDataEntry(value: subject.grades![i].realBewertung, xIndex: count))
            count++
        }
        let set = LineChartDataSet(yVals: dataEntries, label: "Subject")
        set.lineWidth = CGFloat(3)
        set.circleRadius = CGFloat(5.5)
        set.valueFormatter = SetNumberFormatter(set: set)
        set.setCircleColor(UIColorFromRGB(0xf58f2d))
        set.setColor(UIColor.blackColor())
        set.circleHoleColor = UIColorFromRGB(0xf58f2d)
        set.highlightColor = UIColor.blueColor()
        set.highlightLineWidth = CGFloat(1)
        set.drawValuesEnabled = true
        set.valueFont = set.valueFont.fontWithSize(10)
        
        chart.data = LineChartData(xVals: xVals, dataSet: set)
        chart.legend.enabled = false
        chart.descriptionText = ""
        chart.rightAxis.enabled = false
        chart.leftAxis.customAxisMax = 7
        chart.leftAxis.customAxisMin = 0
        chart.leftAxis.valueFormatter = AxisNumberFormatter()
        
        chart.leftAxis.inverted = true
        chart.doubleTapToZoomEnabled = false
        chart.pinchZoomEnabled = false
        
    }
    
    class SetNumberFormatter:NSNumberFormatter {
        
        let set:LineChartDataSet
        var count:NSInteger
        
        init (set: LineChartDataSet){
            self.set = set
            self.count = -1
            super.init()
        }

        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        override func stringFromNumber(number: NSNumber) -> String? {
            count++
            if set.entryCount > 15 {
                if count % 3 == 0 || count == set.entryCount - 1 {
                    if(count == set.entryCount - 1){
                        count = 0
                    }
                    return NSString(format: "%.2f", number as Float) as String
                } else {
                    return ""
                }
            }
            return NSString(format: "%.2f", number as Float) as String
        }
        
    }
    
    class AxisNumberFormatter:NSNumberFormatter {
        
        override func stringFromNumber(number: NSNumber) -> String? {
            return NSString(format: "%.0f", number as Float) as String
        }
        
    }
    
    func getSummary(bewertung: Double) -> String {
    
        if(bewertung < 1.5){
            return "1";
        } else if(bewertung >= 1.5 && bewertung < 2.5){
            return "2";
        } else if(bewertung >= 2.5 && bewertung < 3.5){
            return "3";
        } else if(bewertung >= 3.5 && bewertung < 4.5){
            return "4";
        } else {
            return "5";
        }
    }
    
    func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage {
        
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight))
        image.drawInRect(CGRectMake(0, 0, newWidth, newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
    
    func UIColorFromRGB(rgbValue: UInt) -> UIColor {
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    func showSubject(){
        let alert = UIAlertController(title: "Gegenstand", message: subject.subjectLong, preferredStyle: .Alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler:nil))
        self.presentViewController(alert, animated: true, completion: nil)
    }

}
