//
//  GradeTableViewCell.swift
//  InfoFox-iPhone
//
//  Created by Dominik De Zordo on 15.02.16.
//  Copyright © 2016 Dominik De Zordo. All rights reserved.
//

import UIKit

class GradeTableViewCell: UITableViewCell {

    
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var gradeLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
