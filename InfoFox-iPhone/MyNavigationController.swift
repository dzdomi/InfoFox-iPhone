//
//  MyNavigationController.swift
//  InfoFox-iPhone
//
//  Created by Dominik De Zordo on 08.02.16.
//  Copyright © 2016 Dominik De Zordo. All rights reserved.
//

import UIKit

class MyNavigationController: UINavigationController {

    var token:String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let table = self.topViewController as! SubjectTableViewController
        table.token = token
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    // MARK: - Navigation

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {

        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }

}
