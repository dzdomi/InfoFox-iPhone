//
//  Grade.swift
//  InfoFox-iPhone
//
//  Created by Dominik De Zordo on 05.02.16.
//  Copyright © 2016 Dominik De Zordo. All rights reserved.
//

import Foundation

class Grade {
    var bemerkung:String!
    var read:Bool!
    var timestamp:NSDate!
    var bewertung:String!
    var realBewertung:Double!
    
    init(bemerkung: String, read: Bool, timestamp: NSDate, bewertung: String, realBewertung: Double){
        self.bemerkung = bemerkung
        self.timestamp = timestamp
        self.read = read
        self.bewertung = bewertung
        self.realBewertung = realBewertung
    }
}