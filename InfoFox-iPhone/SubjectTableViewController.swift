//
//  SubjectTableViewController.swift
//  InfoFox-iPhone
//
//  Created by Dominik De Zordo on 04.02.16.
//  Copyright © 2016 Dominik De Zordo. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import LNRSimpleNotifications
import AudioToolbox
import DGElasticPullToRefresh
import NVActivityIndicatorView
import Fluent

class SubjectTableViewController: UITableViewController {

	let notificationManager = LNRNotificationManager()
	var subjects = [Subject]()
    var token:String!
    var loadingView:NVActivityIndicatorView?

	override func viewDidLoad() {
		super.viewDidLoad()
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Logout", style: .Plain, target: self, action:"logout")
        
        let screenSize: CGRect = UIScreen.mainScreen().bounds
        let removeHeight = self.navigationController?.navigationBar.bounds.height
        loadingView = NVActivityIndicatorView(frame: CGRect(x: screenSize.width/2-50, y: screenSize.height/2-50 - removeHeight!, width: 100, height: 100), type: .LineScale, color: UIColor.whiteColor())
        loadingView?.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.75)
        loadingView?.clipsToBounds = true
        loadingView?.layer.cornerRadius = 10
        loadingView?.layer.zPosition = CGFloat(MAXFLOAT)
        loadingView?.startAnimation()
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 60
        tableView.rowHeight = 60
        
		self.notificationManager.notificationsPosition = LNRNotificationPosition.Top
		self.notificationManager.notificationsBackgroundColor = UIColor.redColor()
		self.notificationManager.notificationsTitleTextColor = UIColor.whiteColor()
		self.notificationManager.notificationsBodyTextColor = UIColor.whiteColor()
		self.notificationManager.notificationsSeperatorColor = UIColor.whiteColor()

		let pullToLoadView = DGElasticPullToRefreshLoadingViewCircle()
		pullToLoadView.tintColor = UIColor.whiteColor()
		tableView.dg_addPullToRefreshWithActionHandler({ [weak self]() -> Void in
			self!.loadSubjects()
		}, loadingView: pullToLoadView)
		tableView.dg_setPullToRefreshFillColor((navigationController?.navigationBar.barTintColor)!)
		tableView.dg_setPullToRefreshBackgroundColor(tableView.backgroundColor!)
        
        showProgressBar()
		loadSubjects()
	}

    func logout(){
        let alert = UIAlertController(title: "Logout", message: "Willst du dich wirklich abmelden?", preferredStyle: .Alert)
        alert.addAction(UIAlertAction(title: "Ja", style: .Default, handler:{(UIAlertAction) -> Void in
            self.returnToLogin()
        }))
        alert.addAction(UIAlertAction(title: "Abbrechen", style: .Default, handler: nil))
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}

	override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
		// Only 1 Section in the subjects
		return 1
	}

	override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		// Our dataset
		return subjects.count
	}

	override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCellWithIdentifier("SubjectTableViewCell", forIndexPath: indexPath) as! SubjectTableViewCell
        
		let subject: Subject = subjects[indexPath.row]
		cell.imageView?.image = resizeImage(Subject.getIcon(subject.subjectShort!), newWidth: 40)
		cell.subjectShort.text = subject.subjectShort + " - " + subject.teacher
        cell.subjectLong.text = subject.grades![0].bemerkung + ": " + subject.grades![0].bewertung

        if indexPath.row % 2 == 0 {
            cell.animate(0.5)
                .translateBy(-1000, 0)
        } else {
            cell.animate(0.5)
                .translateBy(1000, 0)
        }
		return cell
	}
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    
    func scaleUIImageToSize(let image: UIImage, let size: CGSize) -> UIImage {
        let hasAlpha = false
        let scale: CGFloat = 0.0 // Automatically use scale factor of main screen
        
        UIGraphicsBeginImageContextWithOptions(size, !hasAlpha, scale)
        image.drawInRect(CGRect(origin: CGPointZero, size: size))
        
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return scaledImage
    }
    
    func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage {
        
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight))
        image.drawInRect(CGRectMake(0, 0, newWidth, newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }

	func loadSubjects() {

		let headers = ["Authorization": token!]
		Alamofire.request(.GET, "http://gfhost.htlstp.ac.at/InfoFox/api/grades", headers: headers)
			.validate()
			.responseJSON { response in switch response.result {
						case .Success(let json):

						self.subjects = [Subject]()

						let response = JSON(json)
						for (_, subJson) in response {
							let subjectShort = subJson["gegKurzbez"]
							let subjectLong = subJson["gegLangbez"]
							let teacher = subJson["teacher"]
							var grades = [Grade]()
							for (_, subsubJson) in subJson["grades"] {
                                let dateFormatter = NSDateFormatter()
                                dateFormatter.dateFormat = "yyyy-MM-dd'T'hh:mm:ssZZZZZ"
                                let date = dateFormatter.dateFromString(subsubJson["lntTimestamp"].string!)
								grades.append(Grade(bemerkung: subsubJson["lntBemerkung"].string!, read: subsubJson["lntGelesen"].bool!, timestamp: date!, bewertung: subsubJson["lntBewertung"].string!, realBewertung: subsubJson["lntRealBewertung"].double!))
							}
                            let subject:Subject = Subject(subjectShort: subjectShort.string!, subjectLong: subjectLong.string!, teacher: teacher.string!, grades: grades)
                            subject.grades!.sortInPlace({$0.timestamp.timeIntervalSince1970 > $1.timestamp.timeIntervalSince1970})
							self.subjects.append(subject)
						}

						self.tableView.reloadData()
						self.tableView.dg_stopLoading()
                        self.hideProgressBar()
						break
						case .Failure(let error):
                        self.notificationManager.showNotification(":(", body: "Fehler beim Laden der Noten", callback: { () -> Void in
                            self.notificationManager.dismissActiveNotification(nil)
                        })
						print(error)
						self.tableView.dg_stopLoading()
                        self.hideProgressBar()
						break
					}
			}
	}

	override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
		if let controller = segue.destinationViewController as? GradeViewController {
			controller.subject = self.subjects[tableView.indexPathForSelectedRow!.row]
		}
	}
    
    func showProgressBar() {
        self.tableView.addSubview(loadingView!)
    }
    
    func hideProgressBar() {
        self.loadingView?.removeFromSuperview()
    }
    
    func returnToLogin(){
        NSUserDefaults.standardUserDefaults().setObject(nil, forKey: "token")
        let board = UIStoryboard(name: "Main", bundle: nil)
        let login = board.instantiateInitialViewController();
        self.presentViewController(login!, animated: true, completion: nil)
    }
    
    func UIColorFromRGB(rgbValue: UInt) -> UIColor {
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}
