//
//  Subject.swift
//  InfoFox-iPhone
//
//  Created by Dominik De Zordo on 04.02.16.
//  Copyright © 2016 Dominik De Zordo. All rights reserved.
//

import Foundation
import UIKit

class Subject {
    
    var subjectShort:String!
    var subjectLong:String!
    var teacher:String!
    var grades:[Grade]?
    
    init () {}
    
    init (subjectShort: String, subjectLong: String, teacher: String, grades: [Grade]){
        self.subjectShort = subjectShort
        self.subjectLong = subjectLong
        self.teacher = teacher
        self.grades = grades
    }
    
    static func getIcon(subject: String) -> UIImage! {
        
        var subjectIcon:String? = nil;
        
        switch subject {
        case "POS1",
        "LOAL",
        "POS1E":
            subjectIcon = "pos"
            break;
        case "DBI1",
        "DBI2":
            subjectIcon = "dbi"
            break;
        case "SYP1":
            subjectIcon = "syp"
            break;
        case "BWM",
        "BWM3":
            subjectIcon = "bwm"
            break;
        case "AM",
        "MAM":
            subjectIcon = "am"
            break;
        case "BESP":
            subjectIcon = "besp"
            break;
        case "DUK",
        "REKO",
        "RHET",
        "D":
            subjectIcon = "deutsch"
            break;
        case "ETH",
        "FEV",
        "FIS",
        "FRK",
        "FR",
        "R",
        "RADV",
        "RAK",
        "RBAP",
        "RBUD",
        "RBUO",
        "RCHG",
        "RE",
        "REFR",
        "REHB",
        "RF",
        "RFCP",
        "RGO",
        "RGRK",
        "RISL",
        "RJLT",
        "RK",
        "RKO",
        "RKPO",
        "RMAZ",
        "RMOS",
        "RNAP",
        "RPGG",
        "RRO",
        "RSO",
        "RSOR",
        "RUMO",
        "RZJE":
            subjectIcon = "rel"
            break;
        case "NVS1":
            subjectIcon = "nvs"
            break;
        case "NW2":
            subjectIcon = "nw"
            break;
        case "SOPK":
            subjectIcon = "sopk"
            break;
        case "TC4A":
            subjectIcon = "tc4a"
            break;
        case "GGP",
        "GGPE":
            subjectIcon = "ggp"
            break;
        case "E",
        "E1":
            subjectIcon = "en"
            break;
        case "TINF":
            subjectIcon = "tinf"
            break;
        default:
            subjectIcon = "unknown"
        }
        
        let image = UIImage(named: subjectIcon!)
        return image
    }
}