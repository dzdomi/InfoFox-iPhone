//
//  SubjectTableViewCell.swift
//  InfoFox-iPhone
//
//  Created by Dominik De Zordo on 04.02.16.
//  Copyright © 2016 Dominik De Zordo. All rights reserved.
//

import UIKit

class SubjectTableViewCell: UITableViewCell {

    
    @IBOutlet weak var subjectIcon: UIImageView!
    @IBOutlet weak var subjectShort: UILabel!
    @IBOutlet weak var subjectLong: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
